#!/bin/bash

platform=$1

prj="`pwd`/`dirname "$0"`"
bin=$prj/bin
libs=$prj/libs
base=$prj/src/os
boot=$prj/system/boot

mkdir -p $bin

# Build the kernel
if [ "$platform" == 'x86' ]; then
    src=$prj/src/x86
    rm -f $libs/*.o && rm -f $src/*.o # clean

    # Libraries
    for filename in `ls $libs | grep "\.c$"`; do
        filename=`echo ${filename/.c/}`;
        gcc -m32 -c $libs/$filename.c -o $libs/$filename.o
    done

    # Build
    echo "Build for x86 (32bits)"
    nasm -f elf32 $src/kernel.asm -o $src/kasm.o
    gcc -m32 -I$libs -c $base/kernel.c -o $src/kc.o
    ld -m elf_i386 -T $src/link.ld -o $boot/kernel.bin $libs/*.o $src/*.o
    rm -f $libs/*.o && rm -f $src/*.o # clean

    # Generate an image
    cp $src/grub.cfg $boot/grub/grub.cfg
    grub-mkrescue -o $bin/gloomos.iso $prj/system

    # Test
    qemu-system-i386 -boot d -cdrom $bin/gloomos.iso -m 512
elif [ "$platform" == 'x64' ]; then
    src=$prj/src/x64
    rm -f $libs/*.o && rm -f $src/*.o # clean

    # Libraries
    for filename in `ls $libs | grep "\.c$"`; do
        filename=`echo ${filename/.c/}`;
        gcc -m64 -c $libs/$filename.c -o $libs/$filename.o
    done

    # Build
    echo "Build for x64 (64bits) {{ IN WORKING }}" && exit
    nasm -f elf64 $src/kernel.asm -o $src/kasm.o
    gcc -m64 -I$libs -c $base/kernel.c -o $src/kc.o
    ld -m elf_x86_64 -T $src/link.ld -o $boot/kernel.bin $libs/*.o $src/*.o
    rm -f $libs/*.o && rm -f $src/*.o # clean

    # Generate an image
    cp $src/grub.cfg $boot/grub/grub.cfg
    grub-mkrescue -o $bin/gloomos.iso $prj/system

    # Test
    qemu-system-x86_64 -boot d -cdrom $bin/gloomos.iso -m 512
else
    echo "Please to execute './build.sh x86' (32bits) or './build x64' (64bits)."
fi
