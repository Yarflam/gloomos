#include "types.h"
#include "screen.h"
#include "keyboard.h"

string prompt(string ask) {
    string response;
    print(ask);
    response = readStr();
    print("\n");
    return response;
}

void kmain() {
    clear();
    /* GloomOS Announce */
    printClr("GloomOS - 32bits", 0xA0);
    printClr(" by Yarflam\n\n", 0x0F);
    print("    =/\\                 /\\=\n");
    print("    / \\'._   (\\_/)   _.'/ \\\n");
    print("   / .''._'--(o.o)--'_.''. \\\n");
    print("  /.' _/ |`'=/ \" \\='`| \\_ `.\\\n");
    print(" /` .' `\\;-,'\\___/',-;/` '. '\\\n");
    print("/.-'       `\\(-V-)/`       `-.\\\n");
    print("`            \"   \"            `\n");
    /* Prompt Test */
    string name;
    while(1) {
        name = prompt("\nWhat's your name ?\n> ");
        print("Hello ");
        print(name);
        print(" !\n");
    }
}
