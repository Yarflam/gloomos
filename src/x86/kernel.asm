bits    32
section         .text
        align   4
        dd      0x1BADB002
        dd      0x00
        dd      - (0x1BADB002+0x00)

global start
extern kmain        ;writed in kernel.c
start:
        cli         ;clear the interrupts
        call kmain  ;processor continue to kmain
        hlt         ;halt the cpu
