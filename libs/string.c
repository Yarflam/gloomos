#include "string.h"

uint16 lenStr(string ch) {
    uint16 i = 1;
    while(ch[i++]);
    return --i;
}

uint16 lenChr(char ch) {
    uint16 i = 0;
    while(ch > 0) {
        ch = (ch - (ch % 10)) / 10;
        i++;
    }
    return i;
}

uint8 eqlStr(string ch1, string ch2) {
    uint8 i;
    uint8 chLen = lenStr(ch1);
    if(chLen != lenStr(ch2)) return 0;
    for(i=0; i <= chLen; i++) {
        if(ch1[i] != ch2[i]) return 0;
    }
    return 1;
}

string ordChr(char ch) {
    string buffStr = "\x00\x00\x00\x00";
    string alpha = "0123456789";
    uint16 iBuffStr = lenChr(ch)-1;
    while(ch > 0 && iBuffStr >= 0 && iBuffStr < 4) {
        buffStr[iBuffStr] = alpha[ch % 10];
        ch = (ch - (ch % 10)) / 10;
        --iBuffStr;
    }
    return buffStr;
}
