#ifndef TYPES_H
#define TYPES_H

typedef signed char int8;
typedef unsigned char uint8;

typedef signed char int16;
typedef unsigned char uint16;

typedef signed char int32;
typedef unsigned char uint32;

typedef signed char int64;
typedef unsigned char uint64;

typedef char* string;

#endif
