#ifndef SCREEN_H
#define SCREEN_H

#include "types.h"

void clearLine(uint8 from, uint8 to);

void updateCursor();

void clear();

void scrollUp(uint8 lnb);

void scrollAuto();

void printChClr(char c, char color);

void printCh(char ch);

void printClr(string ch, char color);

void print(string ch);

#endif
