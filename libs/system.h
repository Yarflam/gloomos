#ifndef SYSTEM_H
#define SYSTEM_H

#include "types.h"

uint8 inPortB(uint16 _port);

void outPortB(uint16 _port, uint8 _data);

#endif
