#include "screen.h"
#include "system.h"
#include "string.h"

int cursorX = 0,
    cursorY = 0,
    textColor = 0x0F;
const uint8 sw = 80, sh = 25, sd = 2;

void clearLine(uint8 from, uint8 to) {
    uint8 x, y;
    string vidmem = (string)0xb8000;
    for(y = from; y < to; y++) {
        for(x = 0; x < sw * sd; x++) {
            vidmem[x + y * sw * sd] = 0x0;
        }
    }
}

void updateCursor() {
    uint16 cursor = (uint16)0x3D4;
    unsigned temp = cursorX + cursorY * sw;
    outPortB(cursor, 14);
    outPortB(cursor, temp >> 8);
    outPortB(cursor, 15);
    outPortB(cursor, temp);
}

void clear() {
    clearLine(0, sh);
    cursorX = 0;
    cursorY = 0;
    updateCursor();
}

void scrollUp(uint8 lnb) {
    uint8 x, y;
    string vidmem = (string)0xb8000;
    if(lnb < 1) return;
    if(lnb >= sh) lnb = sh-1;
    for(y = 0; y < sh-lnb; y++) {
        for(x = 0; x < sw * sd; x++) {
            vidmem[x + y * sw * sd] = vidmem[x + (y + lnb) * sw * sd];
        }
    }
    clearLine(sh-lnb, sh);
    cursorY -= lnb;
    updateCursor();
}

void scrollAuto() {
    if(cursorY >= sh) {
        scrollUp(1);
    }
}

void printChClr(char c, char color) {
    string vidmem = (string)0xb8000;
    switch (c) {
        case 0x08:
            if(cursorX > 0) {
                cursorX--;
                vidmem[(cursorX + cursorY  * sw) * sd] = 0x00;
            }
            break;
        case '\t':
            cursorX += 4 - (cursorX % 5);
            break;
        case '\r':
            cursorX = 0;
            break;
        case '\n':
            cursorX = 0;
            cursorY++;
            break;
        default:
            vidmem[((cursorX + cursorY * sw)) * sd] = c;
            vidmem[((cursorX + cursorY * sw)) * sd + 1] = color;
            cursorX++;
            break;
    }
    if(cursorX >= sw) {
        cursorX = 0;
        cursorY++;
    }
    scrollAuto();
    updateCursor();
}

void printCh(char ch) {
    printChClr(ch, textColor);
}

void printClr(string ch, char color) {
    uint16 i = 0;
    uint16 chLen = lenStr(ch);
    for(i; i < chLen; i++) {
        printChClr(ch[i], color);
    }
}

void print(string ch) {
    printClr(ch, textColor);
}
