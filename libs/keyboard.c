#include "keyboard.h"
#include "system.h"
#include "screen.h"
#include "string.h"

string readStr() {
    const string ascii = "..1234567890....azertyuiop....qsdfghjklm....wxcvbn,;:!...................";
    const string majAscii = "..1234567890....AZERTYUIOP....QSDFGHJKLM....WXCVBN?./§...................";
    char buff;
    uint8 iBuff, reading, found, maj, majLock;
    string buffStr;
    iBuff = 0;
    reading = 1;
    found = 0;
    maj = 0;
    majLock = 0;
    buffStr = "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";
    while(reading && iBuff < 15) {
        if(inPortB(0x64) & 0x1) {
            buff = inPortB(0x60);
            found = 0;
            /* Majuscule */
            if(!found && buff == 42) {
                maj = 1;
                found = 1;
            }
            if(!found && buff == 58) {
                if(majLock) majLock = 0;
                else majLock = 1;
                found = 1;
            }
            /* Space */
            if(!found && buff == 57) {
                printCh(' ');
                buffStr[iBuff] = ' ';
                iBuff++;
                found = 1;
            }
            /* Correct the input */
            if(!found && buff == 14) {
                if(iBuff > 0) {
                    printCh(0x08);
                    buffStr[iBuff-1] = 0x0;
                    iBuff--;
                }
                found = 1;
            }
            /* Confirm the input */
            if(!found && buff == 28) {
                reading = 0;
                found = 1;
            }
            /* Numbers 0-9 */
            if(!found && buff >= 1 && buff <= 11) {
                printCh(ascii[buff]);
                buffStr[iBuff] = ascii[buff];
                iBuff++;
                found = 1;
            }
            /* Alphabet */
            if(!found && buff >= 16 && buff <= 53) {
                if((maj && !majLock) || (!maj && majLock)) {
                    printCh(majAscii[buff]);
                    buffStr[iBuff] = majAscii[buff];
                } else {
                    printCh(ascii[buff]);
                    buffStr[iBuff] = ascii[buff];
                }
                iBuff++;
                maj = 0;
                found = 1;
            }
            /* Debug */
            if(0) {
                print("<");
                print(ordChr(buff));
                print(">");
            }
        }
    }
    buffStr[iBuff] = 0;
    return buffStr;
}
