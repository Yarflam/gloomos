#ifndef STRING_H
#define STRING_H

#include "types.h"

uint16 lenStr(string ch);

uint16 lenChr(char ch);

uint8 eqlStr(string ch1, string ch2);

string ordChr(char ch);

#endif
