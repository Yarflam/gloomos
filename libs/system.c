#include "system.h"

uint8 inPortB(uint16 _port) {
    unsigned char rv;
    __asm__ __volatile__ ("inb %w1,%0" : "=a" (rv) : "dN" (_port));
    return rv;
}

void outPortB(uint16 _port, uint8 _data) {
    __asm__ __volatile__ ("outb %%al,$0x80" : : "dN" (_port), "a" (_data));
}
