# GloomOS

![Capture](capture.png)

[Download the image (5,1 MB)](https://drive.google.com/file/d/1r6Lx27jwj6dpsSrlcazhndJXZdftwrRj/view?fbclid=IwAR2FYFtct_iNPrkSdjoB-Cr-t9xuQI7qwGUnWrnDMgSStvdShzYqRA8L56w)

Or build (Debian/Ubuntu):

```bash
$ git clone https://gitlab.com/Yarflam/gloomos
$ cd gloomos
$ ./setup.sh
$ ./build.sh x86
```

You can use [VirtualBox](https://www.virtualbox.org) to start the image. Select the version `Other/Unknow`.

## Authors

-   Yarflam - _initial worker_

## Sources

[**Video - How to make an operating system from scratch** by _IKnow_](https://www.youtube.com/watch?v=rr-9w2gITDM)
